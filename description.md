Research Scientist

In this position, Dr. Horea-Ioan Ioanas will leverage his background in Free and Open Source Software combined with human and small animal neuroimaging, in order to further the projects pursued by the Center for Open Neuroscience lead by Prof. Halchenko at the Psychological and Brain Sciences Department.
In particular, based on his valuable expertise in hands-on data acquisition and recording/stimulation data integration, he will work within DANDI NIH Brain Initiative Data Archive project (Co-PI Halchenko) to streamline data standardization, analysis, and sharing.
Within the scope of this research position, Dr. Ioanas will complement his data science work with active grant-writing efforts towards enhancing the scope of experimental neuroimaging at Dartmouth.
